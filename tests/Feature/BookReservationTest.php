<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Book;
use Mockery;

class BookReservationTest extends TestCase
{

    use RefreshDatabase;

    /**
     * @test
     */
    public function a_book_can_be_added_to_the_library()
    {
        $response =  $this->post('/api/books',[
            'title' => 'Cool Book Title',
            'author' => 'Victor',
        ]);

        $response->assertOk();

        $this->assertCount(1,Book::all());
    }

    /**
     * @test
     */
    public function a_title_is_required()
    {

        $response =  $this->post('/api/books',[
            'title' => '',
            'author' => 'Victor',
        ]);

        $response->assertSessionHasErrors('title');
    }

    /**
     * @test
     */
    public function a_author_is_required()
    {
        $response =  $this->post('/api/books',[
            'title' => 'Cool Book Title',
            'author' => ' ',
        ]);

        $response->assertSessionHasErrors('author');
    }

    /**
     * @test
     */
     function a_book_can_be_updated()
    {

        $this->withoutExceptionHandling();

        $this->post('/api/books',[
           'title' => 'Cool Title',
           'author' => 'Victor',
        ]);

        $book = Book::first();

        $response = $this->patch('/api/books/' . $book->id ,[
           'title' => 'New Title',
           'author' =>  'New Author',
        ]);

        $this->assertEquals('New Title',Book::first()->title);
        $this->assertEquals('New Author',Book::first()->author);
    }
}
